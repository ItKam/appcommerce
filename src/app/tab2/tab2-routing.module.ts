import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab2Page } from './tab2.page';

const routes: Routes = [
  {
    path: '',
    component: Tab2Page,
  },
  {
    path: 'Subcategory/:id',
    loadChildren: () => import('../subcategory/subcategory.module').then( m => m.SubcategoryPageModule)
  },
  {
    path: 'details/:id',
    loadChildren: () => import('../details/details.module').then( m => m.DetailsPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
