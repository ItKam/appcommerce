import { AppComponent } from './../app.component';
import { Component } from '@angular/core';
import { WoocommerceService } from '../service/woocommerce.service';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {
  
  categories:any;
  loading:boolean;
 
  constructor(public woocommerceService: WoocommerceService, private router: Router, public app: AppComponent, private storage: Storage,) {
   // this.Categorie();
    this.woocommerceService.getObservableForLng().subscribe(() => {
      if(this.router.url === "/tabs/tab2"){
      /*this.storage.get('SELECTED_LANGUAGE').then(val => {
        if(val){
          let lng = val ; 
          if(lng == "de"){
            this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
          }else{
            this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
          }
          console.log("translate", this.woocommerceService.baseUrl);
          this.Categorie();
        }
      });*/

      if(localStorage.getItem('SELECTED_LANGUAGE')){
        let lng = localStorage.getItem('SELECTED_LANGUAGE') ; 
        if(lng == "de"){
          this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
        }else{
          this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
        }
        console.log("translate", this.woocommerceService.baseUrl);
        this.Categorie();
      }
    }
    });
  }

  ngOnInit() {
    //this.Categorie();
  }

  ionViewWillEnter(): void {
    this.Categorie();
    this.app.selected = 2;
  } 

  Categorie(){
    this.loading = true;
    this.woocommerceService.getcategories()
    .subscribe(res => {
      this.categories=res;
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

  openDetailsWithQueryParams(id,name) {
    let navigationExtras ={
      queryParams: {
        special: JSON.stringify(name)
      }
    };
   this.router.navigate([`/tabs/tab2/Subcategory/${id}`], navigationExtras); 
  }

}
