import {Component, OnInit} from '@angular/core';
import {ModalController, NavController, ToastController} from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    myForm: FormGroup;
    email = '';
    password = '';
    public showpassword: boolean;
    constructor(
        private toastCtrl: ToastController,
        public modalController: ModalController,
        public navCtrl: NavController,
        public formBuilder: FormBuilder,
    ) {
    }

    ngOnInit() {
      this.myForm = this.formBuilder.group({
        email: ['', Validators.compose([ Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
    });
    }

    async showToast() {
      const toast = await this.toastCtrl.create({
        message: 'Email or Password incorrect',
        duration: 5000,
        color: 'danger',
        position: 'bottom',
      });
      toast.present();
    }
    togglePasswordText() {
      this.showpassword = !this.showpassword;
  }

    login() {
      this.navCtrl.navigateRoot('/tabs/tab1');
    }
}
