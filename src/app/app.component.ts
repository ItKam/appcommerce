import { LanguageService } from './service/language.service';
import { TranslateModule } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { WoocommerceService } from './service/woocommerce.service';
import { Component } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { Events } from 'Events';
import { TranslateService } from '@ngx-translate/core';
import {Storage} from '@ionic/storage';
import { environment } from '../environments/environment';
import { Tab1Page } from './tab1/tab1.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  token=null;
  selected = 1;
  selectedLng = "fr";
  languageShow: boolean = true;
  languageHide: boolean = false;
  selectedRadioGroup:any;

  languages =[
    {
      text : 'Francais', value: 'fr'
    },
    {
      text : 'Allemand', value: 'de'
    }
  ];

  constructor(
    private storage: Storage,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    public woocommerceService: WoocommerceService,
    private router: Router,
    private emailComposer: EmailComposer,
    private translate: TranslateService,
    private languageService: LanguageService
    
  ) {
    this.initializeApp();
  }

  radioGroupChange(event) {
   //console.log("radioGroupChange",event.detail);
    this.selectedRadioGroup = event.detail;
    console.log(this.selectedRadioGroup);
    this.selectedLng = event.detail.value;
    this.languageService.setLanguage(this.selectedLng);
    this.storage.clear();
    localStorage.removeItem('TOKEN');
    localStorage.removeItem('userConnectedID');
  //  localStorage.clear();
    setTimeout(() => {
      this.woocommerceService.switchlng();
      this.woocommerceService.publishSomeData({
        token: null
      });
    }, 1000);
  }
  
  toggleLanguages(){
    this.languageShow = !this.languageShow;
    this.languageHide = !this.languageHide;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.languageService.setInitialeAppLanguage();
      setTimeout(() => {
        this.languages = this.languageService.getLanguage();
        this.selectedLng = this.languageService.selected;
      }, 1000);
    });
  }
 
  ngOnInit() {
    this.token=this.woocommerceService.getToken();
    //this.languages = this.languageService.getLanguage();
    //this.selectedLng = this.languageService.selected;
    //console.log("selectedLng",this.selectedLng)
  } 

  ionViewWillEnter(): void {
    this.token=this.woocommerceService.getToken();
  }

  sendEmail() {
    if (this.platform.is('cordova')) {
      try {
        this.emailComposer.isAvailable().then((available: boolean) => {
          if (available) {
          }
        });
      
        let email = {
          to: 'info@software-emarkt.de',
          subject: '',
          body: '',
          isHtml: true
        };
        this.emailComposer.open(email);
      } catch(e){  
      }
    }
    else{
      console.log("Platform is not cordova/mobile","You are running in browser, this.emailComposer.isAvailable() is undefined.");
    }
  }
 
  async presentAlertMultipleButtons() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header:  this.translate.instant('TOAST.sure_logout'),
      mode:'ios',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'icon-color',
          handler: () => {
          }
        },
        {
          text: 'Ok',
          cssClass: 'icon-color',
          handler: data => {
            //console.log(data)
            //this.event.
            this.logout();
            this.woocommerceService.publishSomeData({
              token: null
            });
          }
        }
      ]
    });
    await alert.present();
  }

  logout() {
      localStorage.removeItem('TOKEN');
      localStorage.removeItem('userConnectedID');
      this.token = null;
  }
  
  select(val){
    //console.log(this.router.url);
    this.selected = val;
  }

}
