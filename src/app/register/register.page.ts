import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingController, NavController, ToastController} from '@ionic/angular';
import { WoocommerceService } from '../service/woocommerce.service';
@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    myForm: FormGroup;
    isLoading = false;
    error_text = 'duplicate key';
    constructor(
        public formBuilder: FormBuilder,
        public toastController: ToastController,
        public loadingController: LoadingController,
        public woocommerceService: WoocommerceService,
        private navCtrl: NavController,
    ) {
    }

    async loadingDismiss() {
        this.isLoading = false;
        return await this.loadingController.dismiss({
            cssClass: 'custom-loading',
          }).then(() => console.log('loading dismissed'));
    }

    async loadingPresent() {
        this.isLoading = true;
        return await this.loadingController.create({
            cssClass: 'custom-loading',
            message: 'Veuillez patienter...',
            spinner: 'circles'
        }).then(a => {
            a.present().then(() => {
                //console.log('loading presented');
                if (!this.isLoading) {
                    a.dismiss().then(() => console.log('abort laoding'));
                }
            });
        });
    }

    async presentToastsuccess() {
        const toast = await this.toastController.create({
            message: 'Congratulations... An invitation request has been sent for an approval !',
            duration: 5000,
            // TODO: use the color attribute instead of the global override
            color: 'success'
        });
        toast.present();
    }
    async presentToasterror(message) {
        const toast = await this.toastController.create({
            message,
            duration: 5000,
            // TODO: use the color attribute instead of the global override
            color: 'danger'
        });
        toast.present();
    }

    ngOnInit() {
        this.myForm = this.formBuilder.group({
            first_name: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            last_name: ['', Validators.compose([ Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            username: ['', Validators.compose([ Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})[ ]*$'), Validators.required])],
           // confirmemail: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})[ ]*$'), Validators.required])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
            confirmpassword: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
        }, {validator: this.checkIfMatchingPasswords('password', 'confirmpassword')})
    }

    checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
        return (group: FormGroup) => {
            const passwordInput = group.controls[passwordKey],
                passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({notEquivalent: true});
            } else {
                return passwordConfirmationInput.setErrors(null);
            }
        };
    }

    register() {
        const obj = {
            first_name: this.myForm.value.first_name,
            last_name: this.myForm.value.last_name,
            username: this.myForm.value.username,
            email: this.myForm.value.email,
            password: this.myForm.value.password,
        };
         this.loadingController
        .create({
          cssClass: 'custom-loading',
        })
        .then(l => {
          l.present();
        this.woocommerceService.Register(obj)
        .subscribe(res => {
            l.dismiss();
            this.presentToastsuccess();
            this.navCtrl.navigateRoot('tabs/tab4');
        }, err => {
          l.dismiss();
          this.presentToasterror(err.error);
          console.log(err);
        });
      }); 
    }
    
}
