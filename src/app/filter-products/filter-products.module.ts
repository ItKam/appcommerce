import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import {TranslateModule} from "@ngx-translate/core";
import { FilterProductsPageRoutingModule } from './filter-products-routing.module';
import { FilterProductsPage } from './filter-products.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    FilterProductsPageRoutingModule
  ],
  declarations: [FilterProductsPage]
})
export class FilterProductsPageModule {}
