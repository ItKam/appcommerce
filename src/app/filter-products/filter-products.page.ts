import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { WoocommerceService } from './../service/woocommerce.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-filter-products',
  templateUrl: './filter-products.page.html',
  styleUrls: ['./filter-products.page.scss'],
})
export class FilterProductsPage implements OnInit {

  categories:any;
  loading:boolean;
  lowerRang= '10';
  upperRang= '2140';
  categIdSelected: any;
  products: any = [];
  page: number = 1;
  maxPage= 6;

  constructor(public woocommerceService: WoocommerceService, private router: Router,  private loadingCtrl: LoadingController) {  
    this.Categorie();
  }

  ngOnInit() { }
  
  ionViewWillEnter(): void {
    this.Categorie();
  }
  
  changeRang(rang){
   //console.log(rang.value.lower);
    //console.log(rang.value.upper);
    this.lowerRang = rang.value.lower ;
    this.upperRang = rang.value.upper;
  }

  changeSelect(select){
    //console.log(rang.value.lower);
    this.categIdSelected = select.value;
  }


  click(){
    //console.log('done');
  }

  filterByPrice(){

    this.page = 1;
    var parm = '';
    if (this.categIdSelected){
      parm += 'category='+this.categIdSelected+'&';
    }
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
        l.present();
    /* this.woocommerceService.filterByPrice(parm, this.lowerRang, this.upperRang)
    .then(
      res => {
        this.products = res;
        console.log(res);
       // this.maxPage = res['headers'].get('X-WP-TotalPages');
        l.dismiss();
      },
      err => {
        console.log(err)
        l.dismiss();
      }
    ) */
    this.woocommerceService.filterByPrice(parm, this.lowerRang, this.upperRang, this.page)
    .subscribe(res => {
      this.products=res;
      l.dismiss();
      }, err => {
          console.log(err);
          l.dismiss();
      });
    });

  }

  Categorie(){
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
        l.present();
    this.woocommerceService.getcategories()
    .subscribe(res => {
      this.categories=res;
      l.dismiss();
      }, err => {
        console.log(err);
        l.dismiss();
      });
    })
  }

  viewDetails(productId){
    this.router.navigate(['/details/' + productId]);
  }

  loadData(event){
    var parm = '';
    if (this.categIdSelected){
      parm += 'category='+this.categIdSelected+'&';
    }
   // console.log("page"+this.page)
    if(this.page < this.maxPage){
     // console.log(event)
      setTimeout(() => {
        this.woocommerceService.filterByPrice(parm, this.lowerRang, this.upperRang, this.page + 1)
        .subscribe(res => {
          const newList = this.products.concat(res);
          this.products = newList;
          this.page ++; 
          event.target.complete();
          if(res.length === 0){
            this.page = this.maxPage;
          }
        }, err => {
            console.log(err);
        });
      }, 500);
    }else{
      event.target.complete() ;
    }
  }

}
