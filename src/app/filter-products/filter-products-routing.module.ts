import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FilterProductsPage } from './filter-products.page';

const routes: Routes = [
  {
    path: '',
    component: FilterProductsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FilterProductsPageRoutingModule {}
