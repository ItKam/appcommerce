import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FilterProductsPage } from './filter-products.page';

describe('FilterProductsPage', () => {
  let component: FilterProductsPage;
  let fixture: ComponentFixture<FilterProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterProductsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FilterProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
