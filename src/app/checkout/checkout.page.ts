import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from './../tabs/tabs.page';
import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { WoocommerceService } from '../service/woocommerce.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { User } from './../Model/User';
import { Router, ActivatedRoute } from '@angular/router';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { Location } from '@angular/common';
declare var Stripe;

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})

export class CheckoutPage implements OnInit {

  gateways: any;
  stripe = Stripe('pk_test_51HXS6IKc6wTokPhTHcchgHzgXeMkfwKwO4xj6AtVFxRbMBn6yjdaVJLGklUm27l3jSC98H67BgVU9vjJCHpEhuKw00O4fALbfN');
  myForm: FormGroup;
  currentUser: User = null;
  token="";
  product:any;
  products:any;
  name=[];
  price:any;
  productId:any;
  quantity:any;
  methode:any;
  card: any;
  Stripe=true;

  constructor(private translate:TranslateService, public tabs:TabsPage, private location: Location, private router: Router, public navCtrl: NavController,  public toastController: ToastController,private route: ActivatedRoute,public formBuilder: FormBuilder,private loadingCtrl: LoadingController,public woocommerceService: WoocommerceService, public payPal:  PayPal) { }

  ngOnInit() {
 
    this.myForm = this.formBuilder.group({
      first_name: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      last_name: ['', Validators.compose([ Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})[ ]*$'), Validators.required])],
      address_1: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      state: ['', Validators.compose([Validators.required])],
      postcode: ['', Validators.compose([Validators.required])],
      paymentMethod: ['', Validators.compose([Validators.required])],
    });
  
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
      /* this.product = JSON.parse(params.special);
        this.name=this.product.name;
        this.price=this.product.price;
        this.productId=this.product.product_id;
        this.quantity=this.product.quantity;*/
        this.products = JSON.parse(params.special);
        //console.log(this.products)
        this.name=this?.products?.map((u: any) => u.name).join(', ');
        //this.price=this?.products?.map((u: any) => u.price).reduce((a, b) => a + b, 0);
        this.price=this.products.reduce((acc, val) => acc += parseFloat(val.price)*parseInt(val.quantity), 0)+" "+"€";
      }
    });
    this.token=this.woocommerceService.getToken();
    this.getgateway();
    this.getprofile();
  }

  loadStripe() {
    this.Stripe=false;
    let elements = this.stripe.elements();
    var style = {
      base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
          },
        invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };
    this.card = elements.create('card', { style: style });
    this.card.mount('#card-element');
    this.card.addEventListener('change', event => {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
        this.Stripe=false;
      } else  if (event.complete){
        this.Stripe=true;
        displayError.textContent = '';
      }
    });
  }

  getprofile(){
    if(this.token){
      this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
        l.present();
        const userId = localStorage.getItem('userConnectedID');
        this.woocommerceService.getProfile(userId)
        .subscribe(res => {
          this.currentUser = res["billing"];
          this.updateForm();
            l.dismiss();
        }, err => {
          console.log(err)
          l.dismiss();
        });
      });
    }
  }

  updateForm() {

    for (const control of Object.keys(this.myForm.controls)) {
      this.myForm.controls[control].setValue(
          this.currentUser[control]
      );
    }
    this.myForm.markAsUntouched();

  }

  getgateway(){

    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
      l.present();
      this.woocommerceService.getgateway()
      .subscribe(response => {
        this.gateways =response;
       // this.gateways.push({ id: "stripe", title: "Stripe", description: "Payer avec Stripe" });
          l.dismiss();
      }, err => {
        console.log(err)
        l.dismiss();
      });
    });

  }

  async presentToastsuccess() {
    const toast = await this.toastController.create({
        message: this.translate.instant('TOAST.add_order_success'),
        duration: 5000,
        // TODO: use the color attribute instead of the global override
        color: 'success'
    });
    toast.present();
  }

  async presentToasterror(message) {
      const toast = await this.toastController.create({
          message,
          duration: 5000,
          // TODO: use the color attribute instead of the global override
          color: 'danger'
      });
      toast.present();
  }

  onChange($event){
    this.methode=$event.target.value;
    if(this.methode=="stripe"){
      this.loadStripe();
    }else{
      if(this.card){
        this.Stripe=true;
        this.card.unmount();
      }
      var displayError = document.getElementById('card-errors');
      displayError.textContent = '';
    }
  }

  stripePayment( order_id, card, name ) {
		// console.log('stripe payment', order_id, card, name)
	  this.stripe
    .createToken(card, { name }).then(result => {
      if (result.token) {
        console.log("token",result.token);
        this.sendToken( result.token.id, order_id )
      }else if (result.error) {
        console.log("Error creating the token",result.error.message);
      }
    });
  }

  sendToken( token, order_id ) {
		let data = {
			order_id: order_id,
			payment_token: token,
			payment_method: 'stripe'
		}
    this.woocommerceService.stripe(data)
    .subscribe(res => {
      //console.log("res",res)
    }, err => {
      console.log(err);
    });
    // console.log('send token', data)
	}

  doCheckout(){
    const id = localStorage.getItem('userConnectedID');
    let orderItems: any[] = [];
    let data: any = {};
    const billing = {
      first_name:this.myForm.value.first_name,
      last_name:this.myForm.value.last_name,
      email:this.myForm.value.email,
      address_1:this.myForm.value.address_1,
      city:this.myForm.value.city,
      state:this.myForm.value.state,
      postcode:this.myForm.value.postcode,
    };
    var line_items = [];
    this.products.forEach(elm => {
      var objx = {
        product_id: elm.product_id,
        quantity: elm.quantity,
      };
      line_items.push(objx);
    });
    /*const line_items = [{
      product_id: this.productId,
      quantity:this.quantity,
    }];*/
    // console.log(line_items);  
    const shipping_lines = [{
      method_id: this.myForm.value.paymentMethod,
      method_title:this.methode,
      total: this.price,
    }];
      const obj = {
          payment_method: this.myForm.value.paymentMethod,
          payment_method_title:this.methode,
          customer_id: id,
          billing: billing,
          shipping: billing,
          shipping_lines: shipping_lines,
          line_items:line_items,
      };
      data = {
        //Fixed a bug here. Updated in accordance with wc/v2 API
        payment_method: this.myForm.value.paymentMethod,
        payment_method_title: this.methode,
        set_paid: true,
        billing: billing,
        shipping: shipping_lines,
        customer_id: id,
        line_items: line_items
      };
      if (this.methode == "paypal") {
        this.payPal.init({
          PayPalEnvironmentProduction: "AX6IjBsjvH-Ho_ewIfqiDdedXDyBd5AL38Pah12zQ1xb9_2ROkhGkjX8NTQ_nbrei6Y0O6KWlHndKCzs",
          PayPalEnvironmentSandbox: "AYkkS2ObeSpaObaCqA3bybQjRNRMKOw_2vNSha7gmxESpG4l4AhEyMfYwuzrUFKSbWGhCsN-Vhtl5FOG"
        }).then(() => {
          // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
          this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
            // Only needed if you get an "Internal Service Error" after PayPal login!
            //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
          })).then(() => {
              let total = 0.00;
              this.products.forEach(elm => {
                var obj = {
                  product_id: elm.product_id,
                  quantity: elm.quantity,
                };
                orderItems.push(obj);
                total = total + (parseInt(elm.price) * elm.quantity);
              });
                 // orderItems.push({ product_id: this.productId, quantity: this.productId });
                 // total = total + (parseInt(this.price) * this.quantity);
              let payment = new PayPalPayment(total.toString(), 'USD', 'Description', 'sale');
              this.payPal.renderSinglePaymentUI(payment).then((response) => {
                // Successfully paid
                //console.log(response);
                data.line_items = orderItems;
                //console.log(data);
                let orderData: any = {};
                orderData.order = data;
                //console.log(this.methode)
                this.loadingCtrl
                .create({
                  cssClass: 'custom-loading',
                })
                .then(l => {
                  l.present();
                this.woocommerceService.order(orderData.order)
                .subscribe(res => {
                  this.woocommerceService.clearcart()
                  .subscribe(res => {
                      this.presentToastsuccess();
                      l.dismiss();
                      this.navCtrl.back();
                      this.tabs.refreshbadge();
                  }, err => {
                    console.log(err);
                  });
                }, err => {
                  l.dismiss();
                  console.log(err);
                  this.presentToasterror(this.translate.instant('TOAST.add_order_error'));
                });
              }); 
            })
          }, () => {
          });
        }, () => {
      });
    }else{
       this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
        l.present();
        this.woocommerceService.order(obj)
        .subscribe(res => {
          // l.dismiss();
          if (this.methode == "stripe"){
            this.stripePayment((<any>res).id, this.card, billing.first_name)
          }
          this.woocommerceService.clearcart()
          .subscribe(res => {
              this.presentToastsuccess();
              l.dismiss();
              this.tabs.refreshbadge();
              this.navCtrl.back();
          }, err => {
            console.log(err);
          });
        }, err => {
          l.dismiss();
          console.log(err);
          this.presentToasterror(this.translate.instant('TOAST.add_order_error'));
        });
      });
    }
  }

}
