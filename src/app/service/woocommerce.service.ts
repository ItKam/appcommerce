import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from './language.service';
import { AccountPage } from './../account/account.page';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WoocommerceService {

  key: string = "ck_2028707d59ff57fb2450357acc7a4783b7592e4b";
  secret_key: string = "cs_d0bf48c830e9806060ec82b804d6312c35afc363";
  account: AccountPage;
  baseUrl="";
  private mySubject = new Subject<any>();
  private mySubjectForLANG = new Subject<any>();

  constructor(
    private http: HttpClient,private storage: Storage, private languageService:LanguageService,private translate: TranslateService, private router: Router
  ) {
    this.baseUrl = environment.endpoints.baseUrlFrench;
    let lng = ""

    if(localStorage.getItem('SELECTED_LANGUAGE')){
      lng = localStorage.getItem('SELECTED_LANGUAGE') ; 
      if(lng == "de"){
        this.baseUrl = environment.endpoints.baseUrlAllemand;
      }else{
        this.baseUrl = environment.endpoints.baseUrlFrench;
      }
      console.log("translate 11",this.baseUrl);
    }

    /*this.storage.get('SELECTED_LANGUAGE').then(val => {
      if(val){
        lng = val ; 
        if(lng == "de"){
          this.baseUrl = environment.endpoints.baseUrlAllemand;
        }else{
          this.baseUrl = environment.endpoints.baseUrlFrench;
        }

      }
      console.log("translate 11",this.baseUrl);


    });*/

    

    /*this.getObservableForLng().subscribe(() => {
      this.storage.get('SELECTED_LANGUAGE').then(val => {
        if(val){
          lng = val ; 
          if(lng == "de"){
            this.baseUrl = environment.endpoints.baseUrlAllemand;
          }else{
            this.baseUrl = environment.endpoints.baseUrlFrench;
          }
         // this.switchlng();
          console.log("translate",this.baseUrl);
        }
      });
    });*/

  }

  publishSomeData(data: any) {
    this.mySubject.next(data);
  }

  switchlng() {
    this.mySubjectForLANG.next();
  }

  getObservable(): Subject<any> {
    return this.mySubject;
  }


  getObservableForLng(): Subject<any> {
    return this.mySubjectForLANG;
  }

  getToken() {
    return localStorage.getItem('TOKEN')
  }
  /* getProducts(page: number){
    const params = new HttpParams() */
                                   /* .set('consumer_key', this.key)
                                   .set('consumer_secret', this.secret_key) */
                                  /*  .set('per_page', '6')
                                   .set('page', page.toString()) */
   /*  return new Promise((resolve, reject) => {
      this.http.get(this.url + 'wp-json/wc/v2/products',{params, observe: 'response' }).subscribe(data => {
        // Read the result field from the JSON response.
        resolve(data);
      });
    });
  } */

  getProducts(page: number):Observable<any>{
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append('per_page', '6');
    params = params.append('page', page.toString());
    return this.http
    .get(
      this.baseUrl + environment.endpoints.Product+"?status=publish",{ params: params },
    );
  }

  Register(obj):Observable<any>{
    return this.http
    .post(
      this.baseUrl + environment.endpoints.register,obj
    );
  }

  getProduct(id){ 
  /*   const params = new HttpParams().set('consumer_key', this.key)
                                   .set('consumer_secret', this.secret_key) */
    return new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + environment.endpoints.Product+'/'+ id).subscribe(data => {
        // Read the result field from the JSON response.
        resolve(data);
      });
    });
  } 

  loginUser(obj) :Observable<any>{
    return this.http
    .post(
      this.baseUrl + environment.endpoints.login,obj);
  }

  addtocart(product_id, quantity):Observable<any>{
    return this.http
    .post(
      this.baseUrl + environment.endpoints.addtocart,{
        product_id, quantity
    });
  }

  gettocart():Observable<any>{
    return this.http
    .get(
      this.baseUrl + environment.endpoints.cart
    );
  }
  
  deletecart(key){
    return this.http.delete(this.baseUrl + environment.endpoints.deletecart+key);
  }

  clearcart():Observable<any>{
    return this.http
    .post(
      this.baseUrl + environment.endpoints.clearcart,{});
  }


  search(search, page):Observable<any>{
    let params = new HttpParams();
    params = params.append('per_page', '6');
    params = params.append('page', page.toString());
    return this.http
    .get(
      this.baseUrl + environment.endpoints.search+search,{ params: params },
    );
  }
  
  getcategories() :Observable<any>{
    return this.http
    .get(
      this.baseUrl + environment.endpoints.categories,
    );
  }

  getdetailsSubcategory(id, page):Observable<any>{
      
    let params = new HttpParams();
    params = params.append('per_page', '6');
    params = params.append('page', page.toString());
    return this.http
    .get(
      this.baseUrl + environment.endpoints.detailsSubcategory+id,{ params: params },
    );
  }

  filterByPrice(categ, minprice, maxprice, page):Observable<any>{
    
    let params = new HttpParams();

    params = params.append('per_page', '6');
    params = params.append('page', page.toString());

    return this.http
    .get(
      this.baseUrl + environment.endpoints.Product+'?'+categ+'min_price='+minprice+'&max_price='+maxprice+'&status=publish',{ params: params },
    );
  }

  sortproduct(val, categ,page):Observable<any>{
    let params = new HttpParams();
    params = params.append('per_page', '6');
    params = params.append('page', page.toString());

    if (val === "asc" || val === "desc"){
      return this.http
      .get(
        this.baseUrl + environment.endpoints.sort+val+"&category="+categ,{ params: params },
      );
    } else if(val === "title"){
      return this.http
      .get(
        this.baseUrl + environment.endpoints.sort+'asc'+'&orderby=title'+"&category="+categ,{ params: params },
      );
    }else if(val === "price"){
      return this.http
      .get(
        this.baseUrl + environment.endpoints.sort+'asc'+'&orderby=price'+"&category="+categ,{ params: params },
      );
    }else if(val === "price-desc"){
      return this.http
      .get(
        this.baseUrl + environment.endpoints.sort+'desc'+'&orderby=price'+"&category="+categ,{ params: params },
      );
    }else if(val === "stock_quantity"){
      return this.http
      .get(
        this.baseUrl + environment.endpoints.detailsSubcategory+categ+'&stock_status=instock',{ params: params },
      );
    }
  }

  updateUser(id,obj):Observable<any> {
    return this.http
    .put(
      this.baseUrl + environment.endpoints.editprofil+id,obj);
  }

  getShareKey(id):Observable<any>{
    return this.http
    .get(
      this.baseUrl + environment.endpoints.getsharkey+id
    );
  }

  addfav(id, sharekey):Observable<any>{
    const product_id = id;
    return this.http
    .post(
      this.baseUrl + environment.endpoints.wishlist+sharekey+'/add_product',{
        product_id,
      },);
  }

  getProductWishlist(id, offset):Observable<any>{
    let params = new HttpParams();
    params = params.append('count', '6');
    params = params.append('offset', offset.toString());
    return this.http
    .get(
      this.baseUrl + environment.endpoints.wishlist+id+'/get_products',{ params: params },
      );
  }

  getProductWishlistBadge(id):Observable<any>{
    let params = new HttpParams();
    params = params.append('count', '100000000');
    params = params.append('offset', '0');
    return this.http
    .get(
      this.baseUrl + environment.endpoints.wishlist+id+'/get_products',{ params: params },
      );
  }

  deleteProductWishlist(itemID, sharekey){
    const item_id = itemID;
    return this.http
    .get(
      this.baseUrl + environment.endpoints.wishlist+'remove_product/'+item_id
      );
    }

    getProfile(id):Observable<any>{
      return this.http
      .get(
        this.baseUrl + environment.endpoints.profile+id
        );
    }

    getgateway():Observable<any>{
      return this.http
      .get(
        this.baseUrl + environment.endpoints.gateway
      );
    }

  order(obj):Observable<any>{
    return this.http
    .post(
      this.baseUrl + environment.endpoints.order,obj
    );
  }

  stripe(obj):Observable<any>{
    return this.http
    .post(
      //environment.endpoints.proxyurl +
       this.baseUrl + environment.endpoints.stripe,obj
    );
  }

  getorders(userid, page):Observable<any>{

    let params = new HttpParams();

    params = params.append('per_page', '6');
    params = params.append('page', page.toString());

    return this.http
    .get(
      this.baseUrl +environment.endpoints.order+"?customer="+userid ,{ params: params },
    );
  }

  deleteorder(id):Observable<any>{
    return this.http
    .delete(
      this.baseUrl + environment.endpoints.orders+id
    );
  }

  getOrder(id){
    return new Promise((resolve, reject) => {
      this.http.get(this.baseUrl + environment.endpoints.orders+id).subscribe(data => {
        resolve(data);
      });
    });
  }
 

  getBase64ImageFromURL(url: string) {
    return Observable.create(observer => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }

  getBase64Image(img: HTMLImageElement) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }

  refreshAccoutPage(){
    //console.log(this.account)

    this.account.ngOnInit();
    //this.account.token = null;
  }
}
