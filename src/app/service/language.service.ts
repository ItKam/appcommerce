import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

const LNG_KEY = 'SELECTED_LANGUAGE';

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  selected = 'fr';

  constructor(private translate: TranslateService, private storage: Storage) { }

  setInitialeAppLanguage(){
  //  let language = this.translate.getBrowserLang();
    let language = 'fr';
    this.translate.setDefaultLang(language);
    
    if(localStorage.getItem(LNG_KEY)){
      this.setLanguage(localStorage.getItem(LNG_KEY));
      this.selected = localStorage.getItem(LNG_KEY) ; 
    }else{
      this.setLanguage(language);
      this.selected = language ; 
    }

    /*this.storage.get(LNG_KEY).then(val => {
      if(val){
        this.setLanguage(val);
        this.selected = val ; 
      }else{
        this.setLanguage(language);
        this.selected = language ; 
      }
    });*/
  }

  getLanguage(){
    return [
      {
        text : 'FR', value: 'fr'
      },
      {
        text : 'DE', value: 'de'
      }
    ];
  }

  setLanguage(lng){
    this.translate.use(lng);
    this.selected = lng;
   //this.storage.set(LNG_KEY, lng);
    localStorage.setItem(LNG_KEY, lng);
  }
}
