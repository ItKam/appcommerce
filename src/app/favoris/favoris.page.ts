import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from './../tabs/tabs.page';
import { LoadingController, AlertController, ToastController } from '@ionic/angular';
import { WoocommerceService } from './../service/woocommerce.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-favoris',
  templateUrl: './favoris.page.html',
  styleUrls: ['./favoris.page.scss'],
})
export class FavorisPage implements OnInit {

  products=[];
  token="";
  shareKey ="";
  contentloaded = false;
  offset=0;
  maxpage=6;
  page=1; 
  constructor(private storage: Storage,
    private router: Router,
    private route: ActivatedRoute,
    private toast: ToastController,
    public woocommerceService: WoocommerceService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController, 
    public tabs:TabsPage,
    private translate:TranslateService) { 
    this.token=this.woocommerceService.getToken();
  }

  ngOnInit() {
    this.products=[];
    this.contentloaded = false;
   /* setTimeout(() => {
      this.getShareKey();
    }, 1000);*/
   
  }

  ionViewWillEnter(): void {
    this.products=[];
    this.contentloaded = false;
    setTimeout(() => {
      this.getShareKey();
    }, 1000);
  }

  getwishlist(){
    if(this.token){
      //console.log(this.shareKey);
        this.woocommerceService.getProductWishlist(this.shareKey, this.offset)
        .subscribe(res => {
          this.products=res;
          var ind = 0;
          this.products.forEach(prd => {
            prd['price'] += ' €';
            ind +=1;
            this.woocommerceService.getProduct(prd.product_id)
            .then(
              res => {
                prd['info']=res;
                prd['product_image']=res['images'][0].src;
            },)
          });
          if(ind === this.products.length){
            setTimeout(() => {
              this.contentloaded = true;
            }, 4000);
            
          }
        }, err => {
          console.log(err)
         this.contentloaded = true;
        });
    }
  }
   
  loadData(event){
    if(this.page < this.maxpage){
      setTimeout(() => {
        this.woocommerceService.getProductWishlist(this.shareKey, this.offset + 6)
        .subscribe(res => {
          const newList = this.products.concat(res);
          this.products = newList;
          this.page ++; 
          this.offset += 6;
          var ind = 0;
          this.products.forEach(element => {
            ind +=1;
            if(!element.info){
              element['price'] += ' €';
              this.woocommerceService.getProduct(element.product_id)
              .then(
                result => {
                  element['info']=result;
                  element['product_image']=result['images'][0].src;
              },)
            }
          });


      if(ind === this.products.length){
        setTimeout(() => {
          this.contentloaded = true;
        }, 4000);
        
      }

      event.target.complete();
      if(res.length === 0){
        this.page = this.maxpage;
      }
    }, err => {
      console.log(err)
     this.contentloaded = true;
    });
    
      }, 500);
    }
    else{
     event.target.complete();
    }

  }

  loadDefault(event) {
    let img = event.srcElement;
    let actiondiv = document.getElementsByClassName("delete-container");
    let actiondiv2 = document.getElementsByClassName("addcart-container");
    for (var i = 0; i < actiondiv.length; i++) {
      actiondiv[i].setAttribute("style", "opacity: 1;") ;
    }
    for (var i = 0; i < actiondiv2.length; i++) {
      actiondiv2[i].setAttribute("style", "opacity: 1;") ;
    }
    event.target.src = img.src;
  } 

  wishlistDetails(productId){
    let navigationExtras ={
      queryParams: {
        special: JSON.stringify(this.products)
      }
    };
    this.router.navigate(['/tabs/tab1/favoris/details/' + productId], navigationExtras);
  }

  async removeWishlist(product){
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      mode:'ios',
      header: this.translate.instant('TOAST.sure_remove'),
      buttons: [
          {
              text: 'Cancel',
              role: 'cancel',
              cssClass: 'icon-color',
              handler: () => {
              }
          },
          {
              text: 'Ok',
              cssClass: 'icon-color',
              handler: data => {
                this.contentloaded = false;
                    if(this.token){
                    this.woocommerceService.deleteProductWishlist(product.item_id, this.shareKey)
                    .subscribe(res => {
                      this.page = 1;
                      this.offset = 0;
                      this.getwishlist();

                    }, err => {
                        console.log(err);

                    });
                  }
                  if(!this.token){
                    const pid = product.product_id;
                    this.storage.remove(`fav_product_${pid}`);
                    this.getShareKey();
                  }
                
              }
            }
      ]
    });
    await alert.present();
  }

  addtocart(id){
    if(this.token){
      this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
          l.present();
      //console.log(id)
      this.woocommerceService.addtocart(id.toString(),1)
        .subscribe(async res => {
          this.tabs.refreshbadge();
          l.dismiss();
        //  console.log(res);
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.add_cart_success'),
            duration: 5000,
            color: 'success'
          });
          toast.present();
        }, err => {
          console.log(err);
          l.dismiss();
        });
        });
      }else{
        this.storage.get(`product_${id}`).then(async data => {
          if (data) {
            const toast = await this.toast.create({
              message: this.translate.instant('TOAST.add_cart_success'),
              duration: 5000,
              color: 'success'
            });
            toast.present();
            const parsedData = JSON.parse(data);
            parsedData.quantity = parseInt(parsedData.quantity)+1;
            this.storage
            .set(`product_${id}`, JSON.stringify(parsedData))
            .then(() => {
            });
            setTimeout(() => {
              this.tabs.refreshbadge();
            }, 4000);
          } else {
            const toast = await this.toast.create({
              message: this.translate.instant('TOAST.add_cart_success'),
              duration: 5000,
              color: 'success'
            });
            toast.present();
            this.woocommerceService.getProduct(id)
            .then(
              res => {
                var product = res;
                product["quantity"] = 1 ;
                this.storage
                .set(`product_${id}`, JSON.stringify(product))
                .then(() => {
              });
              setTimeout(() => {
                this.tabs.refreshbadge();
              }, 4000);
              //console.log(product)
            },)
           
          }
        });
      }
  }

  getShareKey(){
    if(this.token){
     
          const userId = localStorage.getItem('userConnectedID');
          this.woocommerceService.getShareKey(userId)
          .subscribe(res => {
            const lastItem = res.pop();
            this.shareKey = lastItem['share_key'];
            this.getwishlist();
        }, err => {
          console.log(err);
          this.contentloaded = true;
        });
     
    }else if(!this.token){
      this.products=[];

      var ind = 0
      this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") > -1) {
          const obj = {};
          const parsedData = JSON.parse(value);
          //this.cartData.push(parsedData);
          obj['product_id'] = parsedData.id;
          obj['product_name'] = parsedData.name;
          obj['product_price'] = parsedData.price +" "+"€";
          obj['product_image'] = parsedData.images[0].src;
         
          this.products.push(obj); 
        }
      });
      setTimeout(() => {
        this.contentloaded = true;
      }, 2000);
    }
  }
}

