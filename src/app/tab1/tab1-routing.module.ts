import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Tab1Page } from './tab1.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'details/:id',
    loadChildren: () => import('../details/details.module').then( m => m.DetailsPageModule)
  },
  {
    path: 'favoris',
    loadChildren: () => import('../favoris/favoris.module').then( m => m.FavorisPageModule)
  },
  {
    path: 'filter-products',
    loadChildren: () => import('../filter-products/filter-products.module').then( m => m.FilterProductsPageModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab1PageRoutingModule {}
