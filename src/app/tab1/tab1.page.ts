import { AppComponent } from './../app.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { WoocommerceService } from '../service/woocommerce.service';
import { concat } from 'rxjs/index';
import { Router } from '@angular/router';
import {LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  products: any = [];
  cart=[];
  mywishlist=[];
  page: number = 1;
  maxPage = 6;
  term = '';
  item_count=0;
  like=0;
  token:any;
  shareKey ="";
  infiniteScroll = document.getElementById('infinite-scroll');
  pagewishlist=1;
  offset=0;
  isSearch = false;
  searchValue = "";

  constructor(
    public woocommerceService: WoocommerceService,
    private router: Router,
    private loadingCtrl: LoadingController,
    private storage: Storage, public app: AppComponent
  ) {
    this.woocommerceService.getObservable().subscribe((data) => {
      this.token = data['token'];
      this.item_count=0;
      this.getcart();
    });

    this.woocommerceService.getObservableForLng().subscribe(() => {
      if(this.router.url === "/tabs/tab1"){
     /* this.storage.get('SELECTED_LANGUAGE').then(val => {
        if(val){
          let lng = val ; 
          if(lng == "de"){
            this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
          }else{
            this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
          }
          console.log("translate", this.woocommerceService.baseUrl);
          this.token=this.woocommerceService.getToken();
          this.products= [];
          this.fetchProduct();
          this.item_count=0;
          this.like= 0;
          this.page = 1;
        }
      });
    
    */

      if(localStorage.getItem('SELECTED_LANGUAGE')){
        let lng = localStorage.getItem('SELECTED_LANGUAGE') ; 
        if(lng == "de"){
          this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
        }else{
          this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
        }
        console.log("translate", this.woocommerceService.baseUrl);
        this.token=this.woocommerceService.getToken();
        this.products= [];
        this.fetchProduct();
        this.item_count=0;
        this.like= 0;
        this.page = 1;
      }
   }
    });
  }

  ionViewWillEnter(): void {
    //this.products= [];
    this.cart=[];
    this.app.selected = 1;
    this.token=this.woocommerceService.getToken();
    this.item_count=0;
    this.like= 0;
    this.pagewishlist=1;
    this.offset=0;
    this.isSearch = false;
    this.searchValue = "";
    this.getcart();
    this.getwishlist();
    //this.fetchProduct();
  }
  
  ngOnInit() {
  /*  this.products= [];
    this.cart=[];
    this.like= 0;
    this.pagewishlist=1;
    this.offset=0; 
    this.isSearch = false;
    this.searchValue = "";
    this.fetchProduct();*/
    this.products= [];
    this.fetchProduct();
  }

  fetchProduct(){
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
      showBackdrop:false,
    })
    .then(l => {
      l.present();
      this.woocommerceService.getProducts(this.page)
      .subscribe(res => {
        this.products = res;
        l.dismiss();
      }, err => {
        console.log(err)
        l.dismiss();
      });
    });
  }

  doRefresh(event) {
    this.page = 1;
    this.products= [];
    this.item_count=0;
    this.isSearch = false;
    this.searchValue = "";
    this.fetchProduct();
    this.getcart();
    this.getwishlist();
    event.target.complete();
  }

  getcart(){
    if(this.token){
      this.woocommerceService.gettocart()
      .subscribe(res => {
        this.item_count = res["item_count"];
        //console.log(res)
        console.log(this.item_count)
      }, err => {
        console.log(err)
      });
    }else if(!this.token){
      this.cart=[];
      this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") == -1) {
        const obj = {};
        const parsedData = JSON.parse(value);
        obj['product_id'] = parsedData.id;
        obj['product_name'] = parsedData.name;
        obj['product_price'] = parseInt(parsedData.price);
        obj['quantity'] = parseInt(parsedData.quantity);
        this.cart.push(obj);
        this.item_count += obj['quantity'];
        }
      })
      .then(() => {
      });
    }
  }
  
  loadData(event){
    if(this.page < this.maxPage){
      setTimeout(() => {
        if(this.isSearch){
          this.woocommerceService.search(this.searchValue, this.page + 1)
          .subscribe(res => {
            const newList = this.products.concat(res);
            this.products = newList;
            this.page ++; 
            event.target.complete();
            if(res.length === 0){
              this.page = this.maxPage;
            }
          }, err => {
              console.log(err);
          });
        }else{
          this.woocommerceService.getProducts(this.page + 1)
          .subscribe(res => {
            const newList = this.products.concat(res);
            this.products = newList;
            this.page ++; 
            event.target.complete();
            if(res.length === 0){
              this.page = this.maxPage;
            }
          }, err => {
              console.log(err);
          });
        }
      }, 500);
    }
    else{
     event.target.complete();
    }
  }

  viewDetails(productId){
    let navigationExtras ={
      queryParams: {
        special: JSON.stringify(this.mywishlist)
      }
    };
    this.router.navigate(['/tabs/tab1/details/' + productId], navigationExtras);
  }

  viewCard(){
    this.router.navigate(['/tabs/tab3']);
  }

  searchName(searchValue: string) {
    this.isSearch = true;
    this.searchValue= searchValue;
    this.page = 1;
    this.woocommerceService.search(searchValue, this.page)
    .subscribe(res => {
      this.products=res;
    }, err => {
        console.log(err);
    });
  }

  getwishlist(){
    if(this.token){
      this.mywishlist=[];
      this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
        l.present();
        const userId = localStorage.getItem('userConnectedID');
        this.woocommerceService.getShareKey(userId)
        .subscribe(res => {
          const lastItem = res.pop();
          this.shareKey = lastItem['share_key'];
            this.woocommerceService.getProductWishlistBadge(this.shareKey)
              .subscribe(result => {
                this.mywishlist=result;
                this.like=result.length;
                l.dismiss();
              }, err => {
                console.log(err)
                l.dismiss();
              });
        }, err => {
          console.log(err);
          l.dismiss();
        });  
      });
    }else if(!this.token){
      this.mywishlist=[];
      this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") > -1) {
          const obj = {};
          const parsedData = JSON.parse(value);
          obj['product_id'] = parsedData.id;
          obj['product_name'] = parsedData.name;
          obj['product_price'] = parseInt(parsedData.price);
          this.mywishlist.push(obj);
          this.like=this.mywishlist.length;
        }
      })
      .then(() => {
      });
    }
  }
  
  wishlist(){
    this.router.navigate(['/tabs/tab1/favoris']);
  }

  filterProducts(){
    this.router.navigate(['/tabs/tab1/filter-products']);
  }

}


