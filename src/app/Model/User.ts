/* export class User {

    id: number;
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    
        constructor(id = 0, firstName = '', lastName = '', email ='',  username = '') {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.username = username;
        }
    
    } */

export interface IUser {
    id?: string;
    firstname?: string;
    lastname?: string;
    email?: string;
    password?: string;
    address?: string;
    company?: string;
    phone?: string;
}

export  class User {

    constructor(public  id?: string,
                public firstname?: string,
                public lastname?: string,
                public email?: string,
                public password?: string,
                public address?: string,
                public company?: string,
                public phone?: Coordinates | any
    ) {
    }


    fromData(data: IUser | any) {
        const {
            id,
            firstname,
            lastname,
            email,
            password,
            address,
            company,
            phone,
        } = data;
        return new User(
            id,
            firstname,
            lastname,
            email,
            password,
            address,
            company,
            phone
        );
    }

}

