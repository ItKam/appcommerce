import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WoocommerceService } from './../service/woocommerce.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { User } from './../Model/User';
@Component({
  selector: 'app-edit-profil',
  templateUrl: './edit-profil.page.html',
  styleUrls: ['./edit-profil.page.scss'],
})
export class EditProfilPage implements OnInit {

  firstname="";
  lastname="";
  email="";
  password="";

  user: any;
  address: any;
  currentUser: User = null;
  myForm: FormGroup;

  constructor(public woocommerceService: WoocommerceService, private router: Router,  private loadingCtrl: LoadingController, private route: ActivatedRoute,public formBuilder: FormBuilder,) {
    
   }

  ngOnInit() {
    this.myForm = this.formBuilder.group({
      first_name: ['', Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required]
      )],
      last_name: ['', Validators.compose([
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required]
      )],
      email: ['', Validators.compose([
          Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
          Validators.required]
      )],
     /*  password: ['', Validators.compose([Validators.required, Validators.minLength(8)])], */
      address_1: [''],
      company: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.pattern(/^[+]{0,1}[0-9]{7,20}$/)],
  });
  this.getprofile();

   
}
getprofile(){
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
      l.present();
      const userId = localStorage.getItem('userConnectedID');
      this.woocommerceService.getProfile(userId)
      .subscribe(res => {
        this.currentUser = res["billing"];
        this.updateForm();
          l.dismiss();
      }, err => {
        console.log(err)
        l.dismiss();
      });
    });
}
updateForm() {

  for (const control of Object.keys(this.myForm.controls)) {
      this.myForm.controls[control].setValue(
          this.currentUser[control]
      );
  }

  this.myForm.markAsUntouched();

}
  updateUser(){
    const id = localStorage.getItem('userConnectedID');
        const obj = {
          "billing": {
            first_name: this.myForm.value.first_name,
            last_name: this.myForm.value.last_name,
            email: this.myForm.value.email,
            password: this.currentUser.password,
            address_1: this.myForm.value.address_1,
            phone: this.myForm.value.phone,
            company: this.myForm.value.company,
            id: this.currentUser.id
          },
        };
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
      l.present();
      this.woocommerceService.updateUser(id,obj).subscribe(res => {
        l.dismiss();
    }, err => {
        console.log(err);
        l.dismiss();
    });
  });
  }

}
