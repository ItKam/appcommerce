import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingController, NavController, ToastController} from '@ionic/angular';

@Component({
    selector: 'app-forgot',
    templateUrl: './forgot.page.html',
    styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage implements OnInit {
    myForm: FormGroup;
    email = '';
    id: any;
    constructor(
        public formBuilder: FormBuilder,
        public loadingController: LoadingController,
        public navCtrl: NavController,
        private toast: ToastController
    ) {
    }

    ngOnInit() {
        this.myForm = this.formBuilder.group(
            {
                email: ['', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})[ ]*$'), Validators.required])],
            });
    }
    async showToast(message, color) {
        const toast = await this.toast.create({
          message,
          duration: 5000,
          position: 'bottom',
          color
        });
        toast.present();
      }
      goBack() {
        this.navCtrl.back();
        }
    forgotpassword() {
    }
}
