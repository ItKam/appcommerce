import { TranslateService } from '@ngx-translate/core';
import { LanguageService } from './../service/language.service';
import { TabsPage } from './../tabs/tabs.page';
import { Component } from '@angular/core';
import { WoocommerceService } from '../service/woocommerce.service';
import { Router } from '@angular/router';
import {AlertController,LoadingController} from '@ionic/angular';
import {Storage} from '@ionic/storage';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  products=[];
  name:any;
  token="";
  cartData:any;
  dataService:any;
  baseProducts=[];
  totalprice ="0 €";
  contentloaded = false;

  mywishlist=[];
  shareKey ="";

  constructor(
    private storage: Storage,
    private alertCtrl: AlertController,
    public woocommerceService: WoocommerceService, 
    private router: Router, 
    private loadingCtrl: LoadingController,
    public tabs:TabsPage,
    private translate:TranslateService) {
      this.woocommerceService.getObservableForLng().subscribe(() => {
        if(this.router.url === "/tabs/tab3"){
          /*this.storage.get('SELECTED_LANGUAGE').then(val => {
            if(val){
              let lng = val ; 
              if(lng == "de"){
                this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
              }else{
                this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
              }
              console.log("translate", this.woocommerceService.baseUrl);
              this.token=this.woocommerceService.getToken();
              this.products=[];
              setTimeout(() => {
                this.getcart();
              }, 1000);
            }
          });*/

          if(localStorage.getItem('SELECTED_LANGUAGE')){
            let lng = localStorage.getItem('SELECTED_LANGUAGE') ; 
            if(lng == "de"){
              this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
            }else{
              this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
            }
            console.log("translate", this.woocommerceService.baseUrl);
            this.token=this.woocommerceService.getToken();
              this.products=[];
              setTimeout(() => {
                this.getcart();
              }, 1000);
          }
        }
        
      });
  }

  /*  ngOnInit() {
    this.getcart();
  } */

  ionViewWillEnter(): void {
    this.token=this.woocommerceService.getToken();
    this.products=[];
    if(this.token){
      this.contentloaded = false;
    }else{
      this.contentloaded = true;
    }
    setTimeout(() => {
      this.getcart();
    }, 1000);
    //this.getwishlist();
  }

  getcart(){
    this.totalprice ="0 €";
    this.getwishlist();
    if(this.token){
    
    this.woocommerceService.gettocart()
    .subscribe(res => {
      
      this.totalprice = res.totals.total;
      //console.log(res);
      this.products = res["items"];
      //console.log(this.products)
      var ind = 0;
      this.products.forEach(prd => {
        ind +=1;
        this.woocommerceService.getProduct(prd.product_id)
        .then(
          res => {
            prd['product_image']=res['images'][0].src;
            if(ind === this.products.length){
            
            }
        }, )
      });
      if(ind === this.products.length){
        setTimeout(() => {
          this.contentloaded = true;
        }, 2000);
      }
   
    }, err => {
      console.log(err)
     this.contentloaded = true;
    }); 
  }else if(!this.token){
    this.contentloaded = true;
    this.products=[];
    var totprice = 0;
    this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") === -1) {
          const obj = {};
          const parsedData = JSON.parse(value);
         // this.cartData.push(parsedData);
         totprice += parseFloat(parsedData.price)*parseInt(parsedData.quantity);
         obj['product_id'] = parsedData.id;
         obj['product_name'] = parsedData.name;
         obj['product_price'] =parsedData.price +" "+"€";
         obj['product_image'] = parsedData.images[0].src;
         obj['quantity'] = parsedData.quantity;
         this.products.push(obj); 
         this.totalprice = totprice  +" "+"€";
        }
       /* setTimeout(() => {
          this.contentloaded = true;
        }, 800);*/
      })
      .then(() => {
      });
    }
  }

  viewDetails(productId){
    let navigationExtras ={
      queryParams: {
        special: JSON.stringify(this.mywishlist)
      }
    };
    this.router.navigate(['/tabs/tab3/details/' + productId], navigationExtras);
  }
  
  async removeProduct(product){
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      mode:'ios',
      header: this.translate.instant('TOAST.sure_remove'),
      buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'icon-color',
            handler: () => {
            }
          },
          {
            text: 'Ok',
            cssClass: 'icon-color',
            handler: data => {
              this.contentloaded = false;
                if(this.token){
                  this.woocommerceService.deletecart(product.key)
                  .subscribe(res => {
                    setTimeout(() => {
                      this.getcart();
                      this.tabs.refreshbadge();
                    }, 3000);
                  }, err => {
                      console.log(err);
                  });
                }
                if(!this.token){
                  const pid = product.product_id;
                  this.storage.remove(`product_${pid}`);
                  setTimeout(() => {
                    this.getcart();
                    this.tabs.refreshbadge();
                  }, 3000);
                }
            }
         }
      ]
    });

    await alert.present();  

  }

  goCheckout(products) {
    var listorders= [];
   // console.log(products);
    
    products.forEach(product => {
      var obj={
        key: product.key,
        name:product.product_title,
        price:product.product_price.replace(",", "."),
        product_id:product.product_id,
        quantity:product.quantity,
      }
      listorders.push(obj); 
    });
    //console.log(listorders);
    let navigationExtras ={
      queryParams: {
        special: JSON.stringify(listorders)
      }
    };
    this.router.navigate([`/tabs/tab3/checkout/`], navigationExtras); 
  }

  getwishlist(){
    if(this.token){
      this.mywishlist=[];
        const userId = localStorage.getItem('userConnectedID');
        this.woocommerceService.getShareKey(userId)
        .subscribe(res => {
          const lastItem = res.pop();
          this.shareKey = lastItem['share_key'];
            this.woocommerceService.getProductWishlistBadge(this.shareKey)
              .subscribe(result => {
                this.mywishlist=result;
              }, err => {
                console.log(err)
              });
        }, err => {
          console.log(err);
        });  
    }else if(!this.token){
      this.mywishlist=[];
      this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") > -1) {
          const obj = {};
          const parsedData = JSON.parse(value);
          obj['product_id'] = parsedData.id;
          obj['product_name'] = parsedData.name;
          obj['product_price'] = parseInt(parsedData.price);
          this.mywishlist.push(obj);
        }
      })
      .then(() => {
      });
    }
  }
}
