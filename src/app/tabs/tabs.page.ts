import { LanguageService } from './../service/language.service';
import { TranslateModule } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { WoocommerceService } from './../service/woocommerce.service';
import { Component } from '@angular/core';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})

export class TabsPage {
  token : any ;
  item_count = 0;
  cart = [];
  constructor(public woocommerceService: WoocommerceService, private storage: Storage, private translate: TranslateModule) {
    this.woocommerceService.getObservable().subscribe((data) => {
      this.token = data['token'];
      this.refreshbadge();
    });
  }

  ionViewWillEnter(): void {
    //console.log('refresh');
    //this.item_count = 0;
    this.getcart() 
  }

  getcart(){
    //this.item_count = 0;
    this.token=this.woocommerceService.getToken();
    if(this.token){
      this.woocommerceService.gettocart()
      .subscribe(res => {
        this.item_count = res["item_count"];
        console.log(res)
        console.log(this.item_count)
      }, err => {
        console.log(err)
      });
    }else if(!this.token){
      this.cart=[];
      let qte=0;
      let ind = 0 ;
      this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") == -1) {
        const obj = {};
        const parsedData = JSON.parse(value);
        obj['product_id'] = parsedData.id;
        obj['product_name'] = parsedData.name;
        obj['product_price'] = parseInt(parsedData.price);
        obj['quantity'] = parseInt(parsedData.quantity);
        this.cart.push(obj);
        qte += obj['quantity'];
        ind++;
        
        }
        if(ind === this.cart.length){
          this.item_count = qte;
        }
      })
      .then(() => {
      });
      if(ind === this.cart.length){
        this.item_count = qte;
      }
    }
  }

  refreshbadge(){
    //console.log('refresh badge cart')
    this.getcart()
  }
}
