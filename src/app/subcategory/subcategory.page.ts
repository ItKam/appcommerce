import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WoocommerceService } from '../service/woocommerce.service';
import { ViewEncapsulation } from '@angular/core'
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.page.html',
  styleUrls: ['./subcategory.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SubcategoryPage implements OnInit {
  loading:boolean;
  myId:any;
  products:any;
  name: any;
  page: number = 1;
  maxPage= 6;
  sort_selected = "";
  displayMode: number = 1;
  mywishlist=[];
  token:any;
  shareKey ="";

  constructor( private router: Router,public woocommerceService: WoocommerceService,private route: ActivatedRoute, private storage: Storage) { 
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.name = JSON.parse(params.special);
      }
    });
  }

  ngOnInit() {
    this.token=this.woocommerceService.getToken();
    this.Categorie();
    
  }

  ionViewWillEnter(): void {
    this.getwishlist();
  }

  changeSort(select){
    this.page = 1;
    this.sort_selected = select.value;
    this.loading = true;
    this.woocommerceService.sortproduct(this.sort_selected, this.myId, this.page)
    .subscribe(res => {
      this.products=res;
      this.loading = false;
    }, err => {
        console.log(err);
        this.loading = false;
    });
  }

  loadData(event){
    if(this.sort_selected){
      if(this.page < this.maxPage){
        // console.log(event)
         setTimeout(() => {
           this.woocommerceService.sortproduct(this.sort_selected, this.myId, this.page + 1)
           .subscribe(res => {
             const newList = this.products.concat(res);
             this.products = newList;
             this.page ++; 
             event.target.complete();
             if(res.length === 0){
               this.page = this.maxPage;
             }
           }, err => {
               console.log(err);
           });
         }, 500);
       }
       else{
         event.target.complete();
       }
    }else{
      if(this.page < this.maxPage){
      // console.log(event)
        setTimeout(() => {
          this.woocommerceService.getdetailsSubcategory(this.myId, this.page + 1)
          .subscribe(res => {
            const newList = this.products.concat(res);
            this.products = newList;
            this.page ++; 
            event.target.complete();
            if(res.length === 0){
              this.page = this.maxPage;
            }
          }, err => {
              console.log(err);
          });
        }, 500);
      }
      else{
        event.target.complete();
      }
    }
   }

  Categorie(){
    this.loading = true;
    this.myId = this.route.snapshot.paramMap.get('id');
    this.loading = true;
    this.woocommerceService.getdetailsSubcategory(this.myId, this.page)
    .subscribe(res => {
      console.log(res);
      this. products=res;
      this.loading = false;
    }, err => {
      console.log(err);
      this.loading = false;
    });
  }

  getwishlist(){
    if(this.token){
      this.loading = true;
      this.mywishlist=[];
        const userId = localStorage.getItem('userConnectedID');
        this.woocommerceService.getShareKey(userId)
        .subscribe(res => {
          const lastItem = res.pop();
          this.shareKey = lastItem['share_key'];
            this.woocommerceService.getProductWishlistBadge(this.shareKey)
              .subscribe(result => {
                this.mywishlist=result;
                this.loading = false;
              }, err => {
                console.log(err)
                this.loading = false;
              });
        }, err => {
          console.log(err);
          this.loading = false;
        });  
    }else if(!this.token){
      this.mywishlist=[];
      this.storage
      .forEach((value, key) => {
        if (key.indexOf("fav") > -1) {
          const obj = {};
          const parsedData = JSON.parse(value);
          obj['product_id'] = parsedData.id;
          obj['product_name'] = parsedData.name;
          obj['product_price'] = parseInt(parsedData.price);
          this.mywishlist.push(obj);
        }
      })
      .then(() => {
      });
    }
  }

  viewDetails(productId){
    let navigationExtras ={
      queryParams: {
        special: JSON.stringify(this.mywishlist)
      }
    };
    this.router.navigate(['/tabs/tab2/details/' + productId],navigationExtras);
  }

  onDisplayModeChange(mode: number): void {
    this.displayMode = mode;
  }
}
