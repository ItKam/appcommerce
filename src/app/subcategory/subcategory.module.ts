import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SubcategoryPageRoutingModule } from './subcategoryrouting.module';
import {TranslateModule} from "@ngx-translate/core";
import { SubcategoryPage } from './subcategory.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    SubcategoryPageRoutingModule
  ],
  declarations: [SubcategoryPage]
})
export class SubcategoryPageModule {}
