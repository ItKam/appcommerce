import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SubcategoryPage } from './subcategory.page';

const routes: Routes = [
  {
    path: '',
    component: SubcategoryPage
  },
  {
    path: 'details/:id',
    loadChildren: () => import('../details/details.module').then( m => m.DetailsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubcategoryPageRoutingModule {}
