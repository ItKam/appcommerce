import { TranslateService } from '@ngx-translate/core';
import { AppComponent } from './../app.component';
import { TabsPage } from './../tabs/tabs.page';
import {Component, OnInit} from '@angular/core';
import { LoadingController, ModalController, NavController, ToastController, AlertController } from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { WoocommerceService } from '../service/woocommerce.service';
import {Storage} from '@ionic/storage';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from './../Model/User';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

    myForm: FormGroup;
    public token="";
    userConnected: User;
    currentUser: User = null;
    segmentModel = "profile";
    orders: any = [];
    page: number = 1;
    maxPage = 6;

    public showpassword: boolean;
  
    constructor(
      public tabs:TabsPage,
      private alertCtrl: AlertController,
      private toastCtrl: ToastController,
      public modalController: ModalController,
      public navCtrl: NavController,
      private router: Router,
      public formBuilder: FormBuilder,
      public woocommerceService: WoocommerceService,
      private storage: Storage,
      private loadingCtrl: LoadingController, 
      public app: AppComponent,
      private translate:TranslateService
      
    ) {
      this.woocommerceService.getObservable().subscribe((data) => {
      //  console.log('Data received', data);
        this.token = data['token'];
      });
      this.woocommerceService.getObservableForLng().subscribe(() => {
        if(this.router.url === "/tabs/tab4"){

        /*this.storage.get('SELECTED_LANGUAGE').then(val => {
          if(val){
            let lng = val ; 
            if(lng == "de"){
              this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
            }else{
              this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
            }
            console.log("translate", this.woocommerceService.baseUrl);
            this.logout() 
          }
        });*/

          if(localStorage.getItem('SELECTED_LANGUAGE')){
            let lng = localStorage.getItem('SELECTED_LANGUAGE') ; 
            if(lng == "de"){
              this.woocommerceService.baseUrl = environment.endpoints.baseUrlAllemand;
            }else{
              this.woocommerceService.baseUrl = environment.endpoints.baseUrlFrench;
            }
            console.log("translate", this.woocommerceService.baseUrl);
            this.logout() 
          }
        }
      });
    }

    segmentChanged(event){
      this.page = 1;
      //console.log(this.segmentModel);
      //console.log(event);
      if(this.segmentModel == "orders"){
        this.getorders(this.page);
      }
    }

    loadData(event){
      if(this.page < this.maxPage){
       // console.log(event)
        setTimeout(() => {
          const userId = localStorage.getItem('userConnectedID');
          this.woocommerceService.getorders(userId, this.page + 1)
          .subscribe(res => {
            const newList =  this.orders.concat(res);
            this.orders = newList;
            this.page ++; 
            event.target.complete();
            if(res.length === 0){
              this.page = this.maxPage;
            }
          }, err => {
            console.log(err);
          });
        }, 500);
      }
      else{
        event.target.complete();
      }
    }

    ionViewWillEnter(): void {
      //this.app.selected = 4;
      //this.token=this.woocommerceService.getToken();
      this.getprofile();
     // console.log('WillEnter');
    }
  

    ngOnInit() {
      /*this.myForm = this.formBuilder.group({
        email: ['', Validators.compose([ Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
     });*/
      this.myForm = this.formBuilder.group({
        first_name: ['', Validators.compose([
            Validators.pattern('[a-zA-Z ]*'),
            Validators.required]
        )],
        last_name: ['', Validators.compose([
            Validators.pattern('[a-zA-Z ]*'),
            Validators.required]
        )],
        email: ['', Validators.compose([
            Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'),
            Validators.required]
        )],
        password: ['', Validators.compose([Validators.required, Validators.minLength(8)])],
        address_1: [''],
        company: ['', Validators.compose([Validators.required])],
        phone: ['', Validators.pattern(/^[+]{0,1}[0-9]{7,20}$/)],
      });
      this.token=this.woocommerceService.getToken();
    }

    async showToast() {
      const toast = await this.toastCtrl.create({
        message: this.translate.instant('TOAST.login_error'),
        duration: 5000,
        color: 'danger',
        mode:'ios',
        position: 'bottom',
      });
      toast.present();
    }

    togglePasswordText() {
      this.showpassword = !this.showpassword;
   }

    login() {
        const obj = {
            username: this.myForm.value.email,
            password: this.myForm.value.password,
        };
      this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
        l.present();
      this.woocommerceService.loginUser(obj)
      .subscribe(res => {
        if(res["success"]!=false){
          localStorage.setItem('userConnectedID', res["data"].id);
          this.token=res["data"].token;
          localStorage.setItem('TOKEN', this.token); 
          this.app.token = this.token;
          this.getprofile();
          this.tabs.refreshbadge();
          l.dismiss();
      }else{
      l.dismiss();
      this.showToast();
    }
      }, err => {
        l.dismiss();
        console.log(err);
      });
    });
    }

    getprofile(){
      this.token=this.woocommerceService.getToken();
      if(this.token){
        this.loadingCtrl
        .create({
          cssClass: 'custom-loading',
        })
        .then(l => {
          l.present();
          const userId = localStorage.getItem('userConnectedID');
          this.woocommerceService.getProfile(userId)
          .subscribe(res => {
            this.currentUser = res["billing"];
            this.updateForm();
              l.dismiss();
          }, err => {
            console.log(err)
            l.dismiss();
          });
        });
      }
    }

    updateForm() {
      for (const control of Object.keys(this.myForm.controls)) {
          this.myForm.controls[control].setValue(
              this.currentUser[control]
          );
      }
      this.myForm.markAsUntouched();
  }

  async presentAlertMultipleButtons() {
    const alert = await this.alertCtrl.create({
        cssClass: 'my-custom-class',
        header: this.translate.instant('TOAST.sure_logout'),
        mode:'ios',
        buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'icon-color',
                handler: () => {
                }
            },
            {
                text: 'Ok',
                cssClass: 'icon-color',
                handler: data => {
                    this.logout();
                    // Call you API to remove Items here.
                }
            }
        ]
    });
    await alert.present();
  }

    logout() {
      localStorage.removeItem('TOKEN');
      localStorage.removeItem('userConnectedID');
      this.token='';
      this.tabs.refreshbadge();
    }

    editProfile() {
      this.router.navigate(['/tabs/tab4/edit-profil/']);
    }

    getorders(page){
      this.orders = [];
      this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
        l.present();
        const userId = localStorage.getItem('userConnectedID');
      this.woocommerceService.getorders(userId, page)
      .subscribe(res => {

        this.orders = res;
        l.dismiss();
        //console.log(this.orders)
     
      }, err => {
        l.dismiss();
        console.log(err);
      });
    });
    }

    viewDetails(orderid){
      this.router.navigate(['/tabs/tab4/order/' + orderid]);
    }
    
    async removeOrder(orderid){
      const alert = await this.alertCtrl.create({
        cssClass: 'my-custom-class',
        mode:'ios',
        header: this.translate.instant('TOAST.sure_remove_order'),
        buttons: [
            {
                text: 'Cancel',
                role: 'cancel',
                cssClass: 'icon-color',
                handler: () => {
                }
            },
            {
                text: 'Ok',
                cssClass: 'icon-color',
                handler: data => {
                  this.loadingCtrl
                  .create({
                    cssClass: 'custom-loading',
                  })
                  .then(l => {
                      l.present();
                     
                      this.woocommerceService.deleteorder(orderid)
                      .subscribe(res => {
                        this.page = 1; 
                        this.getorders(this.page);
                        l.dismiss();
                      }, err => {
                          console.log(err);
                          l.dismiss();
                      });
                    
                   
                });
                }
            }
        ]
    });
  
    await alert.present();
     
    }
  }