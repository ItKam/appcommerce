import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountPage } from './account.page';

const routes: Routes = [
  /* {
    path: '',
    component: AccountPage
  } */
  {
    path: '',
    component: AccountPage,
  },
  {
    path: 'register',
    loadChildren: () => import('../register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'forgot',
    loadChildren: () => import('../forgot/forgot.module').then( m => m.ForgotPageModule)
  },
  {
    path: 'edit-profil',
    loadChildren: () => import('../edit-profil/edit-profil.module').then( m => m.EditProfilPageModule)
  },
  {
    path: 'order/:id',
    loadChildren: () => import('../order/order.module').then( m => m.OrderPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class AccountPageRoutingModule {
  
}
