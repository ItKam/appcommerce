import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import {Storage} from '@ionic/storage';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { WoocommerceService } from '../service/woocommerce.service';
import {environment} from '../../environments/environment';
@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  token:any;
  key: string = "ck_2028707d59ff57fb2450357acc7a4783b7592e4b";
  secret_key: string = "cs_d0bf48c830e9806060ec82b804d6312c35afc363";
  constructor(private storage: Storage,public woocommerceService: WoocommerceService,) {
   }


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
   this.token=this.woocommerceService.getToken()
   if ((this.token)&&(!request.url.includes(environment.endpoints.login))) {
    request = request.clone({
          setHeaders: {
          Authorization: "Bearer" +this.token
      }  
 }); 
}else if((!this.token)&&(!request.url.includes(environment.endpoints.login))){
    request = request.clone({
      params: (request.params ? request.params : new HttpParams())
        .set('consumer_key', this.key)
        .set('consumer_secret', this.secret_key)
 }); 
}
  return next.handle(request);
}
}