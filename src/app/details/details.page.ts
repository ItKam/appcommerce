import { TranslateService } from '@ngx-translate/core';
import { TabsPage } from './../tabs/tabs.page';
import { element } from 'protractor';
import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WoocommerceService } from '../service/woocommerce.service';
import {LoadingController,ToastController} from '@ionic/angular';
import {Storage} from '@ionic/storage';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})

export class DetailsPage implements OnInit {

  product: any;
  myId:any;
  token="";
  shareKey ="";
  quantity = 1;
  wishlist=[];

  isFav=false;
  myfavObj: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public woocommerceService: WoocommerceService,
    private loadingCtrl: LoadingController,
    private storage: Storage,
    private toast: ToastController, 
    public tabs:TabsPage,
    private translate: TranslateService
  ) { }

  ngOnInit() {
    this.token=this.woocommerceService.getToken();
   // this.getShareKey();
    this.route.data.subscribe(routeData => {
     let data = routeData['data'];
      if (data) {
        this.product = data;

        //console.log(data)

      }
    });

    if(this.token){
      this.route.queryParams.subscribe(params => {
        if (params && params.special) {
          this.wishlist = JSON.parse(params.special);
        }
      });
      //console.log(this.wishlist)
      this.wishlist.forEach(element => {
        if(element.product_id == this.product.id){
          this.isFav = true;
          this.myfavObj = element;
        }
      });
    }else{
      this.storage.get(`fav_product_${this.product.id}`).then(async data => {
        if (data) {
          this.isFav = true;
        }
      });
    }
  }
  
  AddToCart(){
    if(this.token){
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
      l.present();
      this.myId = this.route.snapshot.paramMap.get('id');
      this.woocommerceService.addtocart(this.myId, this.quantity)
      .subscribe(async res => {
        l.dismiss();
        this.tabs.refreshbadge();
        const toast = await this.toast.create({
          message: this.translate.instant('TOAST.add_cart_success'),
          duration: 5000,
          color: 'success'
        });
        toast.present();
        
      }, err => {
        console.log(err);
        l.dismiss();
      });
      });
    }else{
      this.storage.get(`product_${this.product.id}`).then(async data => {
        if (data) {
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.add_cart_success'),
            duration: 5000,
            color: 'success'
          });
          let parsedData = JSON.parse(data)
          toast.present();
          //console.log(parsedData['quantity'])
          parsedData['quantity'] = parseInt(parsedData.quantity) + this.quantity;
          //console.log(parsedData['quantity'])
          this.storage
          .set(`product_${this.product.id}`, JSON.stringify(parsedData))
          .then(() => {
          });

          setTimeout(() => {
            this.tabs.refreshbadge();
          }, 4000);
          
        } else {
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.add_cart_success'),
            duration: 5000,
            color: 'success'
          });
          toast.present();
         
          this.product["quantity"] = this.quantity;
          this.storage
          .set(`product_${this.product.id}`, JSON.stringify(this.product))
          .then(() => {
          });
          setTimeout(() => {
            this.tabs.refreshbadge();
          }, 4000);
        }
      });
    }
  }

  addfavoris(){
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(l => {
      l.present();
      this.myId = this.route.snapshot.paramMap.get('id');
      this.woocommerceService.addfav(this.myId, this.shareKey)
        .subscribe(async res => {
          l.dismiss();
          this.isFav = true;
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.add_wishlist_success'),
            duration: 5000,
            color: 'success'
          });
          toast.present();
        }, err => {
          console.log(err);
            l.dismiss();
        });
      });
  }

  removefavoris(){
    //console.log('remove fav obj', this.myfavObj)
    this.loadingCtrl
    .create({
      cssClass: 'custom-loading',
    })
    .then(async l => {
      l.present();
        this.woocommerceService.deleteProductWishlist(this.myfavObj.item_id, this.shareKey)
        .subscribe(async res => {
          this.isFav = false;
          l.dismiss();
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.remove_wishlist_success'),
            duration: 5000,
            color: 'success'
          });
          toast.present();
        }, err => {
            console.log(err);
        });
    
      });
  }

  async getShareKey(){
    if(!this.token){
      this.storage.get(`fav_product_${this.product.id}`).then(async data => {
        if (data) {
          const pid = this.product.id;
          this.storage.remove(`fav_product_${pid}`);
  
          this.isFav = false;
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.remove_wishlist_success'),
            duration: 5000,
            color: 'success'
          });
          toast.present();
        } else {
          const toast = await this.toast.create({
            message: this.translate.instant('TOAST.add_wishlist_success'),
            duration: 5000,
            color: 'success'
          });
          this.isFav = true;
          toast.present();
          this.storage
          .set(`fav_product_${this.product.id}`, JSON.stringify(this.product))
          .then(() => {
          });
        }
      });
    }else{
      this.loadingCtrl
      .create({
        cssClass: 'custom-loading',
      })
      .then(l => {
        l.present();
        const userId = localStorage.getItem('userConnectedID');
        this.woocommerceService.getShareKey(userId)
        .subscribe(res => {
            l.dismiss();
           // console.log(res);
           const lastItem = res.pop();
           this.shareKey = lastItem['share_key'];
           if(this.isFav){
            this.removefavoris();
           }else{
            this.addfavoris();
           }
        }, err => {
          console.log(err);
            l.dismiss();
        });
      });
    }
  }

  decreaseQty(){
    if (this.quantity > 1){
      this.quantity -= 1;
    }
  }

  increaseQty(){
    this.quantity += 1;   
  }

}
