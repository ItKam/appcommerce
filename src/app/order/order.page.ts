import { TranslateService } from '@ngx-translate/core';
import { WoocommerceService } from './../service/woocommerce.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Platform, LoadingController, ToastController } from '@ionic/angular';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import * as moment from 'moment';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  order: any; 
  pdfObj = null;
  dataimage: any;
  listName = [];
  listQte = [];
  base64Image = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public woocommerceService: WoocommerceService, 
    private plt: Platform, 
    private file: File, 
    private fileOpener: FileOpener,
    private loadingCtrl: LoadingController,
    private toast: ToastController,
    private translate:TranslateService
    ) {

  }

  ngOnInit() {
   // this.base64Image = [];
    this.route.data.subscribe(routeData => {
      let data = routeData['data'];
      //console.log(data);
      if (data) {
        this.order = data;

       
          this.loadingCtrl
          .create({
            cssClass: 'custom-loading',
          })
          .then(l => {
            l.present();
            //var ind1 = 0 ;
            data.line_items.forEach(element => {
              setTimeout(() => {
            this.woocommerceService.getProduct(element.product_id)
            .then(
              result => {
                element['image'] = result['images'][0].src;
                //ind1++;
                l.dismiss();
              /*   this.woocommerceService.getBase64ImageFromURL('https://cors-anywhere.herokuapp.com/'+element.image).subscribe(base64data => {
                  
                  element['base64Image'] = base64data;
                  if(ind1 === data.line_items.length){
                    l.dismiss();
                  }
                //this.base64Image.push(base64data)
                }); */
               
              },);
             /*  if(ind1 === data.line_items.length){
                l.dismiss();
              } */
            }, 500);
            });
          });
        }
      })
  }


 createPdf() {

    var docDefinition = {
      content: [
        { text: this.translate.instant('BILL.Reçu'), style: 'header' },
        { text: this.translate.instant('BILL.Facture_a'), style: 'title' },
        { text: this.translate.instant('BILL.Nom')+" "+this.order['billing'].last_name, style: 'subheader' },
        { text: this.translate.instant('BILL.Prénom')+" "+this.order['billing'].first_name, style: 'subheader' },
        { text: this.translate.instant('BILL.Adresse')+" "+this.order['billing'].address_1, style: 'subheader' },
        { text: this.translate.instant('BILL.Date')+" "+moment(this.order.date_created).format('LLLL'), style: 'subheader' },
        { text: this.translate.instant('BILL.Modepaiement')+" "+this.order.payment_method, style: 'subheader' },
        {
          style: 'tableExample',
          table: {
            headerRows: 1,
             widths: [300, '*'],
            body: [
              [{text:  this.translate.instant('BILL.Nomproduit'), style: 'tableHeader'}, {text: 'Qte', style: 'tableHeader'}],
              ['', ''],
            ]
          },
          layout: 'headerLineOnly'
        },
      ],
      styles: {
        header: {
          fontSize: 18,
          bold: true,
          alignment: 'center'
        },
        title: {
          fontSize: 14,
          bold: true,
          alignment: 'left',
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        Total: {
          fontSize: 16,
          bold: true,
          alignment: 'right',
          width: '50%',
          color: 'blue'
        } ,
        tableExample: {
          margin: [0, 5, 0, 15]
        },
      }
    };

    
      
        this.loadingCtrl
        .create({
          cssClass: 'custom-loading',
        })
        .then(async l => {
          l.present();
          let ind = 0;
          this.order.line_items.forEach(element => {
         // this.woocommerceService.getBase64ImageFromURL('https://cors-anywhere.herokuapp.com/'+element.image).subscribe(base64data => {
            docDefinition.content.push({
              style: 'tableExample',
              table: {
              headerRows: 1,
              widths: [300, '*'],
              body: [
                [/* {
                  image: 'data:image/jpg;base64,'+element['base64Image'], 
                  width: 80
                },  */
                element.name, 
                element.quantity
                ]
              ]
              },
              layout: 'headerLineOnly'
             }
            ) ;
            ind ++;
            if(ind == this.order.line_items.length){
              l.dismiss();
              docDefinition.content.push({ text: this.translate.instant('BILL.Total')+" "+this.order.total, style: 'Total' },);
              this.pdfObj = pdfMake.createPdf(docDefinition);

              
              
              /*if (this.plt.is('cordova')) {
                this.pdfObj.getBuffer((buffer) => {
                  var blob = new Blob([buffer], { type: 'application/pdf' });
                  // Save the PDF to the data Directory of our App
                  this.file.writeFile(this.file.dataDirectory, 'myletter.pdf', blob, { replace: true }).then(fileEntry => {
                    // Open the PDf with the correct OS tools
                    this.fileOpener.open(this.file.dataDirectory + 'myletter.pdf', 'application/pdf');
                  })
                });
              } else {
                // On a browser simply use download!
                this.pdfObj.download();
              }*/
            }
         // });
        });
    

      //  if(ind == this.order.line_items.length){

          const toast = await this.toast.create({
            message: 'PDF généré',
            duration: 5000,
            color: 'success'
          });
          toast.present();
        //}
    });
  }

  downloadPdf() {

    this.loadingCtrl
        .create({
          cssClass: 'custom-loading',
        })
        .then(l => {
          l.present();
          // this.createPdf();
          if (this.plt.is('cordova')) {
            this.pdfObj.getBuffer((buffer) => {
              var blob = new Blob([buffer], { type: 'application/pdf' });

              // Save the PDF to the data Directory of our App
              this.file.writeFile(this.file.dataDirectory, 'RéceptionDuPaiement.pdf', blob, { replace: true }).then(fileEntry => {
                // Open the PDf with the correct OS tools
                this.fileOpener.open(this.file.dataDirectory + 'RéceptionDuPaiement.pdf', 'application/pdf');
                l.dismiss();
              })
            });
          } else {
            // On a browser simply use download!
            this.pdfObj.download();
            l.dismiss();
          }
        });
  
  }
  
}
