import { Routes, RouterModule } from '@angular/router';
import { OrderResolver } from './order.resolver';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {TranslateModule} from "@ngx-translate/core";
import { IonicModule } from '@ionic/angular';
import { OrderPage } from './order.page';
const routes: Routes = [
  {
    path: '',
    component: OrderPage,
    resolve: {data: OrderResolver}
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderPage],
  providers:[OrderResolver]
})
export class OrderPageModule {}
