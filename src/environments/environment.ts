// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const baseUrlFrench = 'https://software-emarkt.fr/';
const baseUrlAllemand = 'https://software-emarkt.de/';
const proxyurl = "https://cors-anywhere.herokuapp.com/";
export const environment = {
    production: true,
    defaultLanguage: 'en',
    endpoints: {
        baseUrlFrench,
        baseUrlAllemand,
        proxyurl,
        Product: 'wp-json/wc/v2/products',
        login: 'wp-json/jwt-auth/v1/token',
        register: 'wp-json/wc/v3/customers',
        addtocart:  'wp-json/cocart/v1/add-item',
        cart:  'wp-json/cocart/v1/get-cart',
        deletecart: 'wp-json/cocart/v1/item?cart_item_key=',
        clearcart: 'wp-json/cocart/v1/clear',
        search: 'wp-json/wc/v3/products?status=publish&search=',
        categories: 'wp-json/wc/v3/products/categories?parent=0',
        detailsSubcategory: 'wp-json/wc/v3/products?status=publish&category=',
        editprofil: 'wp-json/wc/v3/customers/',
        getsharkey:  'wp-json/wc/v3/wishlist/get_by_user/',
        wishlist: 'wp-json/wc/v3/wishlist/',
        profile: '/wp-json/wc/v2/customers/',
        gateway:  '/wp-json/wc/v2/payment_gateways',
        order: '/wp-json/wc/v3/orders/',
        stripe:  '?wc-api=wc_stripe/',
        orders:  'wp-json/wc/v3/orders/',
        sort: 'wp-json/wc/v3/products?status=publish&order=',
    }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
